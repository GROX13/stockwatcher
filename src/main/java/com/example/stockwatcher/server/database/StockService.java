package com.example.stockwatcher.server.database;

import com.example.stockwatcher.shared.model.Stock;

import javax.ejb.Local;
import java.util.List;

/**
 * StockService uses @Local annotation.
 *
 * Created by george.rokhadze on 9/23/2015.
 */
@Local
public interface StockService {

    Stock returnStockById(Integer stockId);

    List<Stock> returnAllStock();

}
