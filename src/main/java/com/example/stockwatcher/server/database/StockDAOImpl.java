package com.example.stockwatcher.server.database;

import com.example.stockwatcher.shared.model.Stock;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * StockDAOImpl class implements StockDAO interface, uses @Stateless annotation and sets
 * entityManager via @PersistenceContext annotation.For persistence processes, entityManager was used.
 * <p/>
 * Created by george.rokhadze on 9/23/2015.
 */

@Stateless
public class StockDAOImpl implements StockDAO {
    private EntityManager entityManager;

    @PersistenceContext(unitName = "persistence")
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }


    @Override
    public Stock returnStockById(Integer id) {
        return entityManager.getReference(Stock.class, id);
    }

    @Override
    public List<Stock> returnAllStocks() {
        List<Stock> resultList =
                entityManager.createQuery(" select u from      " + Stock.class.getName() + " u").getResultList();

        return resultList;
    }
}
