package com.example.stockwatcher.server.database;

import com.example.stockwatcher.shared.model.Stock;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import java.util.List;

/**
 * HomeManagedBean behaves as backing bean for index.xhtml.
 * It injects UserService via @EJB annotation and returns data
 * from service and assigns them to its variables in init method.
 * init method has @PostConstruct annotation which means the method
 * just runs after HomeManagedBean constructor.
 * <p/>
 * Created by george.rokhadze on 9/23/2015.
 */

@ManagedBean
@RequestScoped
public class HomeManagedBean {

    private StockService stockService;

    @EJB
    public void setStockService(StockService stockService) {
        this.stockService = stockService;
    }

    private Stock stock;
    private List<Stock> list;

    public HomeManagedBean() {

    }

    @PostConstruct
    public void init() {
        stock = stockService.returnStockById(1);
        list = stockService.returnAllStock();
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock user) {
        this.stock = user;
    }

    public List<Stock> getList() {
        return list;
    }

    public void setList(List<Stock> list) {
        this.list = list;
    }

}
