package com.example.stockwatcher.server.database;

import com.example.stockwatcher.shared.model.Stock;

import javax.ejb.Local;
import java.util.List;

/**
 * In StockDAO we use @Local annotation to inject dao object into service
 *
 * Created by george.rokhadze on 9/23/2015.
 */

@Local
public interface StockDAO {

    Stock returnStockById(Integer id);

    List<Stock> returnAllStocks();

}
