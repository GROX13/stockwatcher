package com.example.stockwatcher.server.database;

import com.example.stockwatcher.shared.model.Stock;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;

/**
 * ServiceImpl implements StockService ,uses @Stateless annotation and injects StockDAO using @EJB annotation.
 * <p/>
 * Created by george.rokhadze on 9/23/2015.
 */

@Stateless
public class StockServiceImpl implements StockService {

    private StockDAO stockDAO;

    public StockDAO getStockDAO(){
        return stockDAO;
    }

    @EJB
    public void setStockDAO(StockDAO userDAO){
        this.stockDAO = userDAO;
    }

    @Override
    public Stock returnStockById(Integer stockId) {
        return getStockDAO().returnStockById(stockId);
    }

    @Override
    public List<Stock> returnAllStock() {
        return getStockDAO().returnAllStocks();
    }
}
