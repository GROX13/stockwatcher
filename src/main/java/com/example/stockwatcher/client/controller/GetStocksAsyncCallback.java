package com.example.stockwatcher.client.controller;

import com.example.stockwatcher.shared.model.Stock;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.sencha.gxt.data.shared.ListStore;

import java.util.ArrayList;

/**
 * Async callback.
 *
 * Created by george.rokhadze on 9/22/2015.
 */

public class GetStocksAsyncCallback implements AsyncCallback<ArrayList<Stock>> {
    private ListStore<Stock> store;

    public GetStocksAsyncCallback(ListStore<Stock> store) {
        this.store = store;
    }

    @Override
    public void onFailure(Throwable throwable) {
        Window.alert(throwable.getLocalizedMessage());
    }

    @Override
    public void onSuccess(ArrayList<Stock> stocks) {
        store.addAll(stocks);
    }
}
