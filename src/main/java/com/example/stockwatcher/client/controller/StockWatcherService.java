package com.example.stockwatcher.client.controller;

import com.example.stockwatcher.shared.model.Stock;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import java.util.ArrayList;

@RemoteServiceRelativePath("StockWatcher")
public interface StockWatcherService extends RemoteService {

    void addStock(Stock stock);

    void removeStock(Stock stock);

    ArrayList<Stock> getStocks();

}
