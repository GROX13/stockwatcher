package com.example.stockwatcher.client.controller;

import com.example.stockwatcher.shared.model.Stock;
import com.google.gwt.user.client.rpc.AsyncCallback;

import java.util.ArrayList;

/**
 * The async counterpart of <code>StockWatcherService</code>.
 */

public interface StockWatcherServiceAsync {

    void addStock(Stock stock, AsyncCallback<Void> async);

    void removeStock(Stock stock, AsyncCallback<Void> async);

    void getStocks(AsyncCallback<ArrayList<Stock>> async);
}
