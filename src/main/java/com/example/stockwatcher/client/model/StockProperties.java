package com.example.stockwatcher.client.model;

import com.example.stockwatcher.shared.model.CompanyType;
import com.example.stockwatcher.shared.model.Stock;
import com.google.gwt.editor.client.Editor;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.LabelProvider;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.PropertyAccess;

import java.util.Date;

/**
 * Property Access
 *
 * Created by george.rokhadze on 9/21/2015.
 */
public interface StockProperties extends PropertyAccess<Stock> {
    @Editor.Path("id")
    ModelKeyProvider<Stock> id();

    @Editor.Path("name")
    LabelProvider<Stock> nameLabel();

    ValueProvider<Stock, String> name();

    ValueProvider<Stock, String> symbol();

    ValueProvider<Stock, Double> price();

    ValueProvider<Stock, Double> change();

    ValueProvider<Stock, Double> changePercent();

    ValueProvider<Stock, Date> lastChangeDate();

    ValueProvider<Stock, CompanyType> type();

}
