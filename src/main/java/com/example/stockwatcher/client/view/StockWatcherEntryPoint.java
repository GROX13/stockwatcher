package com.example.stockwatcher.client.view;

import com.example.stockwatcher.client.controller.GetStocksAsyncCallback;
import com.example.stockwatcher.client.controller.StockWatcherService;
import com.example.stockwatcher.client.controller.StockWatcherServiceAsync;
import com.example.stockwatcher.shared.model.CompanyType;
import com.example.stockwatcher.shared.model.Stock;
import com.example.stockwatcher.client.model.StockProperties;
import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.cell.client.DateCell;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.Dialog;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.container.BoxLayoutContainer;
import com.sencha.gxt.widget.core.client.container.CenterLayoutContainer;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.form.FieldLabel;
import com.sencha.gxt.widget.core.client.form.FieldSet;
import com.sencha.gxt.widget.core.client.form.TextField;
import com.sencha.gxt.widget.core.client.grid.ColumnConfig;
import com.sencha.gxt.widget.core.client.grid.ColumnModel;
import com.sencha.gxt.widget.core.client.grid.Grid;
import com.sencha.gxt.widget.core.client.grid.RowNumberer;
import com.sencha.gxt.widget.core.client.selection.SelectionChangedEvent;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Entry Point.
 * <p/>
 * Created by george.rokhadze on 9/21/2015.
 */
public class StockWatcherEntryPoint implements IsWidget, EntryPoint {
    private static final StockProperties properties = GWT.create(StockProperties.class);
    private static final int REFRESH_INTERVAL = 5000;

    private CenterLayoutContainer centerLayoutContainer;
    private static final StockWatcherServiceAsync service = GWT.create(StockWatcherService.class);

    @Override
    public void onModuleLoad() {
        refresh();
    }

    private void refresh() {
        centerLayoutContainer = null;
        RootPanel.get("content").add(asWidget());
    }

    @Override
    public Widget asWidget() {
        if (centerLayoutContainer == null) {
            RowNumberer<Stock> numbererColumn = new RowNumberer<>();

            ColumnConfig<Stock, String> nameCol = new ColumnConfig<>(properties.name(), 200, "Company");
            ColumnConfig<Stock, String> symbolCol = new ColumnConfig<>(properties.symbol(), 100, "Symbol");
            ColumnConfig<Stock, Double> priceCol = new ColumnConfig<>(properties.price(), 75, "Price");
            ColumnConfig<Stock, Double> changeCol = new ColumnConfig<>(properties.change(), 75, "Change");
            ColumnConfig<Stock, Double> changePercentCol = new ColumnConfig<>(properties.changePercent(), 75, "Change Percent");
            ColumnConfig<Stock, Date> lastChangeDateCol = new ColumnConfig<>(properties.lastChangeDate(), 100, "Last Changed");
            ColumnConfig<Stock, CompanyType> typeCol = new ColumnConfig<>(properties.type(), 100, "Type");

            final NumberFormat numberFormatChange = NumberFormat.getFormat("0.00");
            changeCol.setCell(new AbstractCell<Double>() {
                @Override
                public void render(Context context, Double value, SafeHtmlBuilder sb) {
                    String style = "style='color: " + (value < 0 ? "red" : "green") + "'";
                    String v = numberFormatChange.format(value);
                    sb.appendHtmlConstant("<span " + style + " qtitle='Change' qtip='" + v + "'>" + v + "</span>");
                }
            });

            final NumberFormat numberFormatPercent = NumberFormat.getFormat("0.00%");
            changePercentCol.setCell(new AbstractCell<Double>() {
                @Override
                public void render(Context context, Double value, SafeHtmlBuilder sb) {
                    String style = "style='color: " + (value < 0 ? "red" : "green") + "'";
                    String v = numberFormatPercent.format(value);
                    sb.appendHtmlConstant("<span " + style + " qtitle='Change' qtip='" + v + "'>" + v + "</span>");
                }
            });

            lastChangeDateCol.setCell(new DateCell(DateTimeFormat.getFormat("MM/dd/yyyy HH:mm:ss")));

            List<ColumnConfig<Stock, ?>> columns = new ArrayList<>();
            columns.add(nameCol);
            columns.add(symbolCol);
            columns.add(priceCol);
            columns.add(changeCol);
            columns.add(changePercentCol);
            columns.add(lastChangeDateCol);
            columns.add(typeCol);

            ColumnModel<Stock> columnModel = new ColumnModel<>(columns);

            ListStore<Stock> store = new ListStore<>(properties.id());
            service.getStocks(new GetStocksAsyncCallback(store));

            final Grid<Stock> grid = new Grid<>(store, columnModel);
            grid.getView().setAutoExpandColumn(nameCol);
            grid.setBorders(false);
            grid.getView().setStripeRows(true);
            grid.getView().setColumnLines(true);

            // Initialize the row numberer
            numbererColumn.initPlugin(grid);

            final TextButton removeButton = new TextButton("Remove Selected Row(s)");
            removeButton.setEnabled(false);
            SelectEvent.SelectHandler removeButtonHandler = new SelectEvent.SelectHandler() {

                @Override
                public void onSelect(SelectEvent event) {
                    for (Stock stock : grid.getSelectionModel().getSelectedItems()) {
                        service.removeStock(stock, new AsyncCallback<Void>() {
                            @Override
                            public void onFailure(Throwable throwable) {
                                Window.alert("Something went wrong");
                            }

                            @Override
                            public void onSuccess(Void aVoid) {
                                refresh();
                            }
                        });
//                        grid.getStore().remove(stock);
                    }
                    removeButton.setEnabled(false);
                }
            };
            removeButton.addSelectHandler(removeButtonHandler);

            grid.getSelectionModel().addSelectionChangedHandler(new SelectionChangedEvent.SelectionChangedHandler<Stock>() {
                @Override
                public void onSelectionChanged(SelectionChangedEvent<Stock> event) {
                    removeButton.setEnabled(!event.getSelection().isEmpty());
                }
            });

            TextButton addButton = new TextButton("Add New Entry");
            addButton.addSelectHandler(new SelectEvent.SelectHandler() {
                @Override
                public void onSelect(SelectEvent event) {
                    createForm();
                }
            });

            TextButton refreshButton = new TextButton("Refresh Table");
            refreshButton.addSelectHandler(new SelectEvent.SelectHandler() {
                @Override
                public void onSelect(SelectEvent event) {
                    refresh();
                }
            });

            ContentPanel panel = new ContentPanel();

            panel.setHeadingText("Stock Watcher");
            panel.setPixelSize(700, 400);
            panel.setButtonAlign(BoxLayoutContainer.BoxLayoutPack.END);
            panel.addButton(addButton);
            panel.addButton(removeButton);
            panel.addButton(refreshButton);
            panel.setWidget(grid);

            centerLayoutContainer = new CenterLayoutContainer();
            centerLayoutContainer.add(panel);
        }
        return centerLayoutContainer;
    }

    private void createForm() {
        final Dialog dialog = new Dialog();
        dialog.setHeadingText("Adding New Stock");

        final TextField companyName = new TextField();
        companyName.setAllowBlank(false);

        final TextField symbol = new TextField();
        symbol.setAllowBlank(false);

        final TextField price = new TextField();
        price.setAllowBlank(false);

        VerticalLayoutContainer vlc = new VerticalLayoutContainer();
        vlc.add(new FieldLabel(companyName, "Company Name"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));
        vlc.add(new FieldLabel(symbol, "Symbol"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));
        vlc.add(new FieldLabel(price, "Price"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));

        FieldSet fieldSet = new FieldSet();
        fieldSet.setHeadingText("Stock Information");
        fieldSet.setCollapsible(true);
        fieldSet.add(vlc);

        dialog.add(fieldSet);
        dialog.setPredefinedButtons();
        dialog.setBodyStyle("background: none; padding: 10px");
        dialog.addButton(new TextButton("Save", new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {

                service.addStock(
                        new Stock(companyName.getText(), symbol.getText(),
                                Double.valueOf(price.getText()), CompanyType.UNKNOWN), new AsyncCallback<Void>() {
                            @Override
                            public void onFailure(Throwable throwable) {
                                Window.alert("Something went wrong!");
                            }

                            @Override
                            public void onSuccess(Void aVoid) {
                                refresh();
                                dialog.hide();
                            }
                        });
            }
        }));
        dialog.addButton(new TextButton("Cancel", new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                dialog.hide();
            }
        }));
        dialog.show();
    }

}
