package com.example.stockwatcher.shared.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * Stock database.
 * <p/>
 * Created by george.rokhadze on 9/21/2015.
 */

@Entity
@Table(name = "stocks")
public class Stock implements Serializable {

    private static int COUNTER = 0;

    @Id
    @Column(name ="id")
    private Integer id;

    @Column(name ="company")
    private String name;

    @Column(name ="symbol")
    private String symbol;

    @Column(name ="price")
    private Double price;

    @Column(name ="price_change")
    private Double change;

    @Column(name ="change_date")
    private Date lastChangeDate;

    @Column(name ="company_type")
    private CompanyType type;

    public Stock() {
        this.id = COUNTER++;
        updateDate();
    }

    public Stock(String name, String symbol, Double price, CompanyType type) {
        this();
        setName(name);
        setSymbol(symbol);
        this.price = price;
        this.change = 0D;
        setType(type);
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        updateDate();
        this.change = this.price - price;
        this.price = price;
    }

    public Double getChange() {
        return change;
    }

    public double getChangePercent() {
        return 100.0 * this.change / this.price;
    }

    public Date getLastChangeDate() {
        return lastChangeDate;
    }

    public CompanyType getType() {
        return type;
    }

    public void setType(CompanyType type) {
        this.type = type;
    }


    private void updateDate() {
        this.lastChangeDate = new Date();
    }

}
