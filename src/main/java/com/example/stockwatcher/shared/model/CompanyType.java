package com.example.stockwatcher.shared.model;

/**
 * Fields of companies.
 *
 * Created by george.rokhadze on 9/21/2015.
 */
public enum CompanyType {
    AUTO,
    MEDIA,
    MEDICAL,
    UNKNOWN, TECH
}
